<?php
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

// Membuat instance dari class Animal (diturunkan ke Frog dan Ape)
$my_animal = new Animal("Shaun");
echo "Animal name: " . $my_animal->name . "<br>";
echo "Number of legs: " . $my_animal->legs . "<br>";
echo "Cold blooded: " . ($my_animal->cold_blooded ? 'yes' : 'no') . "<br><br>";

// Membuat instance dari class Frog
$my_frog = new Frog("Buduk");
echo "Frog name: " . $my_frog->name . "<br>";
echo "Number of legs: " . $my_frog->legs . "<br>";
echo "Cold blooded: " . ($my_frog->cold_blooded ? 'yes' : 'no') . "<br>";
$my_frog->jump(); // Memanggil method jump()

// Membuat instance dari class Ape
$my_ape = new Ape("Kera Sakti");
echo "<br><br> Ape name: " . $my_ape->name . "<br>";
echo "Number of legs: " . $my_ape->legs . "<br>";
echo "Cold blooded: " . ($my_ape->cold_blooded ? 'yes' : 'no') . "<br>";
$my_ape->yell(); // Memanggil method yell()