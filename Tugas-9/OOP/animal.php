<?php

class Animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = false; // Assuming false means 'no' for cold-blooded

    public function __construct($name) {
        $this->name = $name;
    }
}