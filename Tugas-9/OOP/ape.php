<?php

require_once 'animal.php';

class Ape extends Animal {
    public $legs = 2; // Override jumlah kaki dari class Animal

    public function yell() {
        echo "Auooo\n";
    }
}