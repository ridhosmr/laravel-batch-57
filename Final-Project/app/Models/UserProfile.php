<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserProfile extends Pivot
{
    protected $table = 'user_profiles';

    protected $guarded = [];
}
