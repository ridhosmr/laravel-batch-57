<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Genre;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::with('genre')->get();
        
        return view('pages.movies.index', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::all();
        return view('pages.movies.create', compact('genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'release_date' => 'required|date',
            'description' => 'required',
            'genre_id' => 'required|exists:genres,id',
        ]);

        DB::transaction(function () use ($validated) {
            Movie::create([
                'title' => $validated['title'],
                'release_date' => $validated['release_date'],
                'description' => $validated['description'],
                'genre_id' => $validated['genre_id'],
            ]);
        });

        Alert::success('Sukses', 'Data telah ditambahkan');

        return redirect()->route('movies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        $movie->load('genre');

        $reviews = Review::with('user', 'genre', 'movie')->where('movie_id', $movie->id)->get();

        return view('pages.movies.show', compact('movie', 'reviews'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        $genres = Genre::all();
        return view('pages.movies.edit', compact('movie', 'genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'release_date' => 'required|date',
            'description' => 'required',
            'genre_id' => 'required|exists:genres,id',
        ]);

        DB::transaction(function () use ($validated, $movie) {
            $movie->update([
                'title' => $validated['title'],
                'release_date' => $validated['release_date'],
                'description' => $validated['description'],
                'genre_id' => $validated['genre_id'],
            ]);
        });

        Alert::success('Sukses', 'Data telah diubah');

        return redirect()->route('movies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        Alert::success('Sukses', 'Data telah dihapus');
        $movie->delete();
        return redirect()->route('movies.index');
    }
}
