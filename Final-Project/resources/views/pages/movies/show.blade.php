@extends('layouts.master')

@section('content')
<section class="table-components">
    <div class="container-fluid">
        @section('title', 'Detail Movie')
        
        <div class="tables-wrapper">
            <div class="row">
            <div class="col-lg-12">
                <div class="card-style mb-30">
                    <table class="table">
                        <tr>
                            <th>Judul</th>
                            <td>{{ $movie->title }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Rilis</th>
                            <td>{{ $movie->release_date }}</td>
                        </tr>
                        <tr>
                            <th>Deskripsi</th>
                            <td>{{ $movie->description }}</td>
                        </tr>
                        <tr>
                            <th>Genre</th>
                            <td>{{ $movie->genre->name }}</td>
                        </tr>
                    </table>
                    <a href="{{ route('movies.index') }}" class="btn btn-secondary mt-3">Kembali</a>
                </div>
                <div class="row">
                    @foreach($reviews as $review)
                    <div class="col-md-6 col-lg-4 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $review->user->name }}</h5>
                                <div class="rating mb-2">
                                    @for($i = 1; $i <= 5; $i++)
                                        @if($i <= $review->rating)
                                            <span class="text-warning"><i class="lni lni-star-fill"></i></span>
                                        @else
                                            <span class="text-muted"><i class="lni lni-star-fill"></i></span>
                                        @endif
                                    @endfor
                                </div>
                                <p class="card-text">{{ $review->review_text }}</p>
                                @if($review->review_note)
                                    <p class="card-text"><small class="text-muted">Catatan: {{ $review->review_note }}</small></p>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="card-style mb-30">
                    <h4>Tulis Review</h4>
                    <form action="{{ route('reviews.store') }}" method="POST" class="mt-4">
                        @csrf
                        <div class="form-group mt-3">
                            <label for="rating">Rating</label>
                            <div class="rating">
                                <input type="radio" id="star1" name="rating" value="1" /> <label for="star1" title="1 star">★</label><br>
                                <input type="radio" id="star2" name="rating" value="2" /> <label for="star2" title="2 stars">★★</label><br>
                                <input type="radio" id="star3" name="rating" value="3" /> <label for="star3" title="3 stars">★★★</label><br>
                                <input type="radio" id="star4" name="rating" value="4" /> <label for="star4" title="4 stars">★★★★</label><br>
                                <input type="radio" id="star5" name="rating" value="5" /> <label for="star5" title="5 stars">★★★★★</label>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <label for="review_text">Review</label>
                            <textarea name="review_text" id="review_text" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group mt-3">
                            <label for="review_note">Catatan</label>
                            <textarea name="review_note" id="review_note" class="form-control" rows="2"></textarea>
                        </div>
                        <input type="hidden" name="movie_id" value="{{ $movie->id }}">
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="genre_id" value="{{ $movie->genre->id }}">
                        <button type="submit" class="btn btn-primary mt-3">Kirim Review</button>
                    </form>
                </div>                
            </div>
            </div>
        </div>
    </div>
</section>
@endsection
