@extends('layouts.master')

@section('content')
<section class="table-components">
    <div class="container-fluid">
        @section('title', 'Tambah Movie')
        
        <div class="tables-wrapper">
            <div class="row">
            <div class="col-lg-12">
                <div class="card-style mb-30">
                <form action="{{ route('movies.store') }}" method="POST">
                    @csrf
                    <div class="form-group mt-3">
                        <label for="title">Judul</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>
                    <div class="form-group mt-3">
                        <label for="release_date">Tanggal Rilis</label>
                        <input type="date" class="form-control" id="release_date" name="release_date" required>
                    </div>
                    <div class="form-group mt-3">
                        <label for="description">Deskripsi</label>
                        <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
                    </div>
                    <div class="form-group mt-3">
                        <label for="genre_id">Genre</label>
                        <select class="form-control" id="genre_id" name="genre_id" required>
                            <option value="">Pilih Genre</option>
                            @foreach($genres as $genre)
                                <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Simpan</button>
                    <a href="{{ route('movies.index') }}" class="btn btn-secondary mt-3">Kembali</a>
                </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
@endsection
