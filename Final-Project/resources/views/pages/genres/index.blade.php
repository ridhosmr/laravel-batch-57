@extends('layouts.master')

@section('content')
<section class="table-components">
    <div class="container-fluid">
        @section('title', 'Genre')
        
        <div class="tables-wrapper">
            <div class="row">
            <div class="col-lg-12">
                <div class="card-style mb-30">
                <h6 class="mb-10">
                    <a href="{{ route('genres.create') }}" class="btn btn-primary mb-3">Tambah Data</a>
                </h6>
                <div class="table-wrapper table-responsive-sm">
                    <table id="example" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($genres as $genre)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $genre->name }}</td>
                                <td class="text-center">
                                    <a href="{{ route('genres.show', $genre->id) }}" class="btn btn-info "><i class="mdi mdi-eye-outline"></i></a>
                                    <a href="{{ route('genres.edit', $genre->id) }}" class="btn btn-warning"><i class="mdi mdi-pencil-outline"></i></a>
                                    <form action="{{ route('genres.destroy', $genre->id) }}" method="POST" style="display: inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger"><i class="mdi mdi-delete-outline"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
<script src="https://cdn.datatables.net/2.0.8/js/dataTables.bootstrap5.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $('form').submit(function(event) {
        event.preventDefault();
        Swal.fire({
            title: 'Apakah Anda yakin?',
            text: "Data yang dihapus tidak dapat dikembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                $(this).unbind('submit').submit();
            }
        });
    });
</script>
@endpush
