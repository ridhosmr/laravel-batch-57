@extends('layouts.master')

@section('content')
<section class="table-components">
    <div class="container-fluid">
        @section('title', 'Detail Genre')
        
        <div class="tables-wrapper">
            <div class="row">
            <div class="col-lg-12">
                <div class="card-style mb-30">
                    <h3>{{ $genre->name }}</h3>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Judul Film</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($genre->movie->isEmpty())
                                <tr>
                                    <td colspan="2">Tidak ada film</td>
                                </tr>
                            @else
                                @foreach($genre->movie as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->title }}</td>
                                </tr>
                                @endforeach
                            @endif                        </tbody>
                    </table>
                    <a href="{{ route('genres.index') }}" class="btn btn-secondary mt-3">Kembali</a>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
@endsection
