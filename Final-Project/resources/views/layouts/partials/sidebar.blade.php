<aside class="sidebar-nav-wrapper">
<div class="navbar-logo">
    <a href="index.html">
    <img src="{{ asset('assets/images/logo/logo.svg') }}" alt="logo" />
    </a>
</div>
<nav class="sidebar-nav">
    <ul>
    <li class="nav-item">
        <a href="{{ route('home') }}">
            <span class="icon me-3">
            <i class="mdi mdi-view-dashboard"></i>
        </span>
        <span class="text">Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('genres.index') }}">
            <span class="icon me-3">
            <i class="mdi mdi-movie"></i>
        </span>
        <span class="text">Genre</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('movies.index') }}">
            <span class="icon me-3">
            <i class="mdi mdi-play-circle"></i>
        </span>
        <span class="text">Movie</span>
        </a>
    </li>
    </ul>
</nav>
</aside>
