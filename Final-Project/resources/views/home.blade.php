@extends('layouts.master')


@section('content')
<section class="table-components">
    <div class="container-fluid">
    @section('title', 'Home')
        
      <!-- ========== tables-wrapper start ========== -->
      <div class="tables-wrapper">
        <div class="row">
          <div class="col-lg-12">
            <div class="card-style mb-30">
                <p>Anda telah login sebagai {{ Auth::user()->name }}. Selamat datang di aplikasi review film.</p>
            </div>
            <!-- end card -->
          </div>
          <!-- end col -->
        </div>
        <!-- end row -->
      </div>
      <!-- ========== tables-wrapper end ========== -->
    </div>
    <!-- end container -->
  </section>
@endsection
